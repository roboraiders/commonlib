package org.team1817;

import edu.wpi.first.wpilibj.Timer;

abstract class Subsystem implements Runnable {

    private int state;
    private final int DISABLED = 0;
    private final int ENABLED = 1;

    private final double DELAY = 0.001;
    private long timeBetweenLoop;

    /**
     * Generic constructor for Subsystem class.
     */
    public Subsystem() {
    }

    /**
     * Overridden run() method from Runnable. Runs until JVM stops. There are two
     * states that are checked ENABLED runs actions() DISABLED runs haltSystem()
     */
    public void run() {
        long startTime = System.currentTimeMillis();
        this.timeBetweenLoop = 0;

        while (!Thread.interrupted()) {
            switch (state) {
            case ENABLED:
                actions();
                break;
            case DISABLED:
                haltSystem();
                break;
            }
            
            long endTime = System.currentTimeMillis();
            this.timeBetweenLoop = endTime - startTime;
            startTime = endTime;

            Timer.delay(this.DELAY);
        }
    }

    /**
     * Generic subsystem start
     */
    protected void start() {
        start("Subsystem");
    }

    /**
     * Named subsystem start
     */
    protected void start(String name) {
        this.state = this.DISABLED;
        new Thread(this, name).start();
    }

    /**
     * @return How many seconds the main loop took
     */
    protected double loopTime() {
        return (double) (this.timeBetweenLoop) / 1000.0;
    }

    /**
     * Enables the thread (calls actions() every loop)
     */
    public void enable() {
        this.state = this.ENABLED;
    }

    /**
     * Disables the thread (calls haltSystem() every loop)
     */
    public void disable() {
        this.state = this.DISABLED;
    }

    /**
     * The actions that should be taken every iteration
     */
    abstract void actions();

    /**
     * This method should stop ALL motors associated with the subsystem
     */
    abstract void haltSystem();
}
