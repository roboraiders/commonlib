package org.team1817.motion;

import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class Drive extends AbstractDrive {

    private final DifferentialDrive drive;

    public Drive(DifferentialDrive drive, PowerDistributionPanel pdp, int maxCurrent, int[] motors, double timeToMax) {
        super(pdp, maxCurrent, motors, timeToMax);
        start("Drive");
        this.drive = drive;
        this.param1.enable();
        this.param2.enable();
    }

    @Override
    protected void actions() {
        double param1Power = param1.getOutput();
        double param2Power = param2.getOutput();

        if(arcadeControls)
            this.drive.arcadeDrive(param1Power, param2Power, false);
        else
            this.drive.tankDrive(param1Power, param2Power, false);
    }

    @Override
    protected void haltSystem() {
        this.drive.stopMotor();

        param1.haltSystem();
        param2.haltSystem();
    }




}
