package org.team1817.motion;

import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

class SingleJointSubsystem extends AbstractSubsystem {

    public SingleJointSubsystem(PowerDistributionPanel pdp, int maxCurrent, int[] motors, double timeToMax, SpeedControllerGroup controllers) {
        super(pdp, maxCurrent, motors, timeToMax, controllers);
    }

    protected void actions() {
        this.controllers.set(this.param1.getOutput());
    }

    protected void haltSystem() {
        this.param1.setOutput(0);
        this.controllers.stopMotor();
    }

}