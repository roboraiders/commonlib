package org.team1817.motion;

import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

abstract class AbstractSubsystem extends Subsystem {

    protected TrapezoidalMotion param1;
    protected SpeedControllerGroup controllers;

    /**
     * @param pdp
     * @param maxCurrent
     * @param motors
     * @param timeToMax
     * @param controllers The speed controller group for all the motor(s) of the subsystem
     */
    public AbstractSubsystem(PowerDistributionPanel pdp, int maxCurrent, int[] motors, double timeToMax, SpeedControllerGroup controllers) {
        Power powerInfo = new Power(pdp, maxCurrent, motors);
        this.param1 = new TrapezoidalMotion(timeToMax, powerInfo);
        this.controllers = controllers;
        start("Abstract Subsystem");
        this.param1.enable();
    }

    abstract void actions();
    
    abstract void haltSystem();

    /**
     * Sets the percent output target.
     * This will use trapezoidal motion.
     * @param target The percent output (from -1 to +1)
     */
    public void setOutputTarget(double target) {
        this.param1.setTarget(target);
    }

    /**
     * Sets the precent output.
     * This will not use trapezoidal motion.
     * @param output The percent output (from -1 to +1)
     */
    public void setOutput(double output) {
        this.param1.setOutput(output);
    }

}