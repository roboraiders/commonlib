package org.team1817;

class TrapezoidalMotion extends Subsystem implements Utils {

    private double timeToMax;
    private double output;
    private double outputTarget;

    private Power powerInfo;

    /**
     * @param timeToMax How long the robot should take to go from 0 - 1 (neutral to
     *                  full speed) in seconds
     * @param powerInfo The Power class responsible for current and voltage limiting
     */
    public TrapezoidalMotion(double timeToMax, Power powerInfo) {
        this.timeToMax = timeToMax;
        this.powerInfo = powerInfo;
        this.output = 0.0;
        this.outputTarget = 0.0;
        start("Trapezoidal Motion");
    }

    private double calculateChange(double init, double target, double delta) {
        double inc = Math.abs(target) >= Math.abs(init) + delta ? delta : Math.abs(init) - Math.abs(target);

        if (inc >= 0 && target - init < 0 || inc < 0 && init < 0)
            inc *= -1;

        init += inc;

        // Normalize output
        init = clamp(init);

        return init;
    }

    /**
     * Updates the output power bounded between -1 and +1
     */
    private void updateSpeeds() {
        double delta = calculateDelta(); // Max change
        
        this.output = calculateChange(this.output, this.outputTarget, delta);
    }

    /**
     * Calculates the change in power based on the current output, target output,
     * and power limitations
     * 
     * @return The amount of change the current output should change by
     */
    private double calculateDelta() {
        // Formula: (Change in time) / (time to max)
        if (this.powerInfo.limit())
            return -(this.loopTime() / this.timeToMax) / 2; // Back off the power by half of the normal change

        return this.loopTime() / this.timeToMax;
    }

    /**
     * @return How long, in seconds, it takes to go from zero to one
     */
    public double getTimeToMax() {
        return this.timeToMax;
    }

    /**
     * Sets how long it takes to go from zero to one
     * @param newTime Amount of time, in seconds
     */
    public void setTimeToMax(double newTime) {
        this.timeToMax = newTime;
    }

    /**
     * What the output value is, from -1 to +1
     * @return The output value
     */
    public double getOutput() {
        return this.output;
    }

    /**
     * Sets the output value
     * @param output The immediately required output value
     */
    public void setOutput(double output) {
        this.output = clamp(output);
    }

    /**
     * @param newTarget The desired output
     */
    public void setTarget(double newTarget) {
        this.output = clamp(newTarget);
    }

    /**
     * @return The desired output
     */
    public double getTarget() {
        return this.outputTarget;
    }

    /**
     * Sets the output and target to zero
     */
    public void stop() {
        this.setTarget(0.0);
        this.setOutput(0.0);
    }

    @Override
    protected void actions() {
        this.updateSpeeds();
    }

    @Override
    protected void haltSystem() {
        this.stop();
    }

}