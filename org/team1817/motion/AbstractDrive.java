package org.team1817.motion;

import edu.wpi.first.wpilibj.PowerDistributionPanel;

abstract class AbstractDrive extends Subsystem {

    protected TrapezoidalMotion param1;
    protected TrapezoidalMotion param2;
    protected boolean arcadeControls;

    public AbstractDrive(PowerDistributionPanel pdp, int maxCurrent, int[] motors, double timeToMax) {
        Power powerInfo = new Power(pdp, maxCurrent, motors);
        this.param1 = new TrapezoidalMotion(timeToMax, powerInfo);
        this.param2 = new TrapezoidalMotion(timeToMax, powerInfo);
        this.arcadeControls = true;
    }

    /**
     * This method is the voltage ramped and power wall limited arcade drive.
     * You should use this method if you want arcade drive controls.
     * @param forward
     * @param rotation
     */
    public void arcadeDrive(double forward, double rotation) {
        param1.setTarget(forward);
        param2.setTarget(rotation);
        this.arcadeControls = true;
    }

    /**
     * This method is the voltage ramped and power wall limited tank drive.
     * You should use this method if you want tank drive control.
     * @param leftPower
     * @param rightPower
     */
    public void tankDrive(double leftPower, double rightPower) {
        param1.setTarget(leftPower);
        param2.setTarget(rightPower);
        this.arcadeControls = false;
    }

    /**
     * This method should only be used if you want raw inputs without 
     * voltage ramping and power limiting. You should probably use 
     * arcadeDrive() instead.
     * @param forward
     * @param rotation
     */
    public void directArcadeDrive(double forward, double rotation) {
        param1.setOutput(forward);
        param2.setOutput(rotation);
        this.arcadeControls = true;
    }

    /**
     * This method should only be used if you want raw inputs without
     * voltage ramping and power limiting. You should probably use
     * tankDrive() instead.
     * @param leftPower
     * @param rightPower
     */
    public void directTankDrive(double leftPower, double rightPower) {
        param1.setOutput(leftPower);
        param2.setOutput(rightPower);
        this.arcadeControls = false;
    }

    abstract void actions();

    abstract void haltSystem();

}