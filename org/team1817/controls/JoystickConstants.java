package org.team1817;

import edu.wpi.first.wpilibj.GenericHID; 

class JoystickConstants {

    public static final int RIGHT_HAND = GenericHID.Hand.kRight;
    public static final int LEFT_HAND = GenericHID.Hand.kLeft;
    
    public static final int POV_UP = 0;
    public static final int POV_DOWN = 180;
    public static final int POV_LEFT = 90;
    public static final int POV_RIGHT = 270;

}