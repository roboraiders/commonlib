package org.team1817;

import edu.wpi.first.wpilibj.PowerDistributionPanel;

class Power{

    private final PowerDistributionPanel pdp;
    private int maxCurrent;
    private int[] motor;

    private final double MIN_VOLTAGE = 8.5;

    public Power(PowerDistributionPanel pdp) {
        this(pdp, 100);
    }

    public Power(PowerDistributionPanel pdp, int maxCurrent) {
        this(pdp, maxCurrent, new int[0]);
    }

    /**
     * @param pdp The Power Distribution panel on the robot
     * @param maxCurrent The ceiling for power draw 
     * @param motors The ports populated by a specific subsystem. If no ports are provided, the entire PDP will be monitored.
     */
    public Power(PowerDistributionPanel pdp, int maxCurrent, int[] motors) {
        this.pdp = pdp;
        this.maxCurrent = maxCurrent;
        this.motors = motors;
    }

    /**
     * Finds the summation of the current being pulled by the drive motors
     * 
     * @return The total number of amps drawn by the drivetrain
     */
    private double reportCurrentDraw() {
        double currentCurrent = 0;
        
        if(motors.length > 0) {
            for(int motor : motors)
                currentCurrent += this.pdp.getCurrent(motor);
        } else {
            currentCurrent = this.pdp.getTotalCurrent();
        }

        return currentCurrent;
    }

    /**
     * @return True if the drivetrain is pulling more than the allowed amount of
     *         amps.
     */
    public boolean overCurrent() {
        return reportCurrentDraw() > this.maxCurrent;
    }

    /**
     * @return True if the battery is below a 8.5 volts
     */
    public boolean underVoltage() {
        return this.pdp.getVoltage() < this.MIN_VOLTAGE;
    }

    /**
     * @return If the bot is over current limit or under voltage limit
     */
    public boolean limit() {
        return this.overCurrent() || this.underVoltage();
    }

}