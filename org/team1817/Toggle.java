package org.team1817;

public class Toggle {

	private boolean flip;
	private boolean state;

	public Toggle() {
        this(false);
	}

	public Toggle(boolean value) {
		this.state = value;
		this.flip = false;
	}

	/**
	 * Uses the low-side of button presses for performing actions
	 * 
	 * @param value
	 *            The button to be monitored
	 * @return The state of the action
	 */
	public boolean update(boolean value) {
		if (value) {
			this.flip = true;
		} else if (flip) {
			this.flip = false;
			this.state = !this.state;
		}

		return this.state;
	}

	/**
	 * Manually sets the state of the action
	 * 
	 * @param value
	 *            The state wished to be set
	 */
	public void set(boolean value) {
		this.state = value;
	}

	/**
	 * @return The state of the action
	 */
	public boolean get() {
		return this.state;
	}
}