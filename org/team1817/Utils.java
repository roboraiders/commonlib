package org.team1817;

public interface Utils {

    public final double MAX_INPUT = 1.0;

    /**
     * This method clamps an input value between positive and negative one
     * 
     * @param input The value to be clamped
     * @return The clamped value
     */
    default double clamp(double input) {
        if (Math.abs(input) > MAX_INPUT)
            return input > MAX_INPUT ? MAX_INPUT : -MAX_INPUT;

        return input;
    }

    /**
     * This method clamps an input value between a specified high and low value
     * If the min parameter is larger than the max parameter, the input parameter will be returned
     * 
     * @param input The value to be clamped
     * @param max   The largest possible value
     * @param min   The smallest possible value
     * @return The clamped value or input value if min is larger than max
     */
    default double clamp(double input, double max, double min) {
        if (min > max)
            return input; // Error if the min is larger than the max
        if (input > max)
            return max;
        if (input < min)
            return min;
        return input;
    }

}
